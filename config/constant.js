export const menu = [
  {
    href: '/landing-page-1',
    title: 'Home One '
  },
// eslint-disable-next-line semi
]

export const alumniTerbaik = [
  {
    href: '/alumniTerbaik',
    title: 'Alumni Terbaik '
  },
// eslint-disable-next-line semi
]

export const proyekPerubahan = [
  {
    href: '/proyekPerubahan',
    title: 'Proyek Perubahan '
  },
// eslint-disable-next-line semi
]

export const pengumuman = [
  {
    href: '/pengumuman',
    title: 'Pengumuman '
  },
// eslint-disable-next-line semi
]

export const ikapimnas = [
  {
    href: '/tentang',
    title: 'Tentang'
  },
  {
    href: '/susunan',
    title: 'Susunan Organisasi'
  },
]
export const pages = [
  {
    href: '/our-team',
    title: 'Our Team'
  },
  {
    href: '/clients',
    title: 'Clients'
  },
  {
    href: '/careers',
    title: 'Careers'
  },
  {
    href: '/contact-us',
    title: 'Contact Us'
  }
]
export const landing1About = [
  {
    href: '/about-1',
    title: 'About-1'
  },
  {
    href: '/about-2',
    title: 'About-2'
  }
]
export const landing2About = [
  {
    href: '/about-1',
    title: 'About-1'
  }
]
export const ourCases = [
  {
    href: '/masonry-2',
    title: 'Masonry 2 Portfolio'
  },
  {
    href: '/masonry-3',
    title: 'Masonry 3 Portfolio'
  },
  {
    href: '/masonry-no-space',
    title: 'Masonry No Space'
  },
  // {
  //   href: 'portfolio-detail',
  //   title: 'Portfolio Detail'
  // },
  {
    href: '/portfolio-2',
    title: 'Portfolio 2 columns'
  },
  {
    href: '/portfolio-3',
    title: 'Portfolio 3 columns'
  },
  {
    href: '/portfolio-4',
    title: 'Portfolio 4 columns'
  },
  {
    href: '/portfolio-5',
    title: 'Portfolio 5 columns'
  },
  {
    href: '/portfolio-no-space',
    title: 'Portfolio No Space'
  }
]
export const blog = [
  {
    href: '/blog',
    title: 'Blog'
  },
  {
    href: '/blog-one-column-grid',
    title: 'One Column Grid'
  },
  {
    href: '/blog-two-column-grid',
    title: 'Two Column Grid'
  },
  {
    href: '/blog-three-column-grid',
    title: 'Three Column Grid'
  },
  {
    href: '/blog-right-sidebar-grid-1',
    title: 'Blog Right Sidebar Grid 1'
  },
  {
    href: '/blog-right-sidebar-grid-2',
    title: 'Blog Right Sidebar Grid 2'
  },
  {
    href: '/blog-left-sidebar-grid-1',
    title: 'Blog Left Sidebar Grid 1'
  },
  {
    href: '/blog-left-sidebar-grid-2',
    title: 'Blog Left Sidebar Grid 2'
  },
  {
    href: '/blog-details',
    title: 'Blog Detail'
  }
]
