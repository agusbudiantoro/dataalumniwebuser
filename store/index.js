import Axios from 'axios'
import Vuex from 'vuex'
import Vue from 'vue'
import { reject } from 'lodash';
import { axios } from '~/nuxt.config';

Vue.use(Vuex)
var ip9093 ="https://dev-smartbangkom.lan.go.id:9093/";
var ip9092 ="https://dev-smartbangkom.lan.go.id:9092/";

const createStore = () => {
    return new Vuex.Store({
        strict: false,
        state: {
            
        },
        getters:{
            
        },
        mutations: {
           
        },
        actions: {
            // pengumuman
            getPengumuman() {
                return new Promise((resolve, reject) => {
                    Axios.get(ip9093 + 'Pengumuman', {
                    }).then(res => {
                        resolve(res)
                        console.log('sukses');
                    })
                })
            },
            // end pengumuman

            //get gambar
            getGambar(dispatch,gbr){
                return new Promise((resolve, reject) => {
                    console.log(gbr);
                    Axios.post(ip9093 + 'Pengumuman/gambarori/'+gbr, {
                    }).then(res => {
                        resolve(res.data)
                        console.log('sukses');
                    })
                })
            },
            // end get gambar

            // pengumuman
        getPengumuman() {
            return new Promise((resolve, reject) => {
                Axios.get(ip9093 + 'Pengumuman', {
                }).then(res => {
                    resolve(res)
                    console.log('sukses');
                })
            })
        },
        // end pengumuman

        // get proper
        getProper() {
            return new Promise((resolve, reject) => {
                Axios.get(ip9092 + 'proper', {
                }).then(res => {
                    resolve(res)
                    console.log('sukses');
                })
            })
        },
        // end get proper

        // get proper pagination
        getProperPaginasi(dispatch,page) {
            return new Promise((resolve, reject) => {
                Axios.get(ip9092 + 'proper/allproper/'+page+'/6', {
                }).then(res => {
                    resolve(res)
                    console.log('sukses');
                })
            })
        },
        // end get proper pagination

        // get cari proper pagination
        getCariProperPaginasi(dispatch,isi) {
            return new Promise((resolve, reject) => {
                Axios.get(ip9092 + 'proper/cariproper/'+isi.page+'/6?judul='+isi.judul, {
                }).then(res => {
                    resolve(res)
                    console.log('sukses');
                })
            })
        },
        // end get cari proper pagination

        // get alumni
        getAlumni(dispatch,id) {
            return new Promise((resolve, reject) => {
                Axios.get(ip9092 + 'alumni/' +id, {
                }).then(res => {
                    resolve(res)
                    console.log('sukses');
                })
            })
        },
        // end get alumni

        // get alumni terbaik
        getAlumniTerbaik() {
            return new Promise((resolve, reject) => {
                Axios.get(ip9093 + 'alumniTerbaik/getallbydiklat', {
                }).then(res => {
                    resolve(res)
                    console.log('sukses');
                })
            })
        },
        // end get alumni terbaik

        // get foto alumni
        getFotoAlumni(dispatch,gbr) {
            return new Promise((resolve, reject) => {
                Axios.post(ip9092 + 'alumni/gambarori/' +gbr, {
                }).then(res => {
                    resolve(res)
                    console.log('sukses');
                })
            })
        },
        // end get foto alumni
        }
    })
}

export default createStore