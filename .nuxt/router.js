import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _8e04bf32 = () => interopDefault(import('..\\pages\\alumniTerbaik\\index.vue' /* webpackChunkName: "pages/alumniTerbaik/index" */))
const _7c220820 = () => interopDefault(import('..\\pages\\landing-page-1\\index.vue' /* webpackChunkName: "pages/landing-page-1/index" */))
const _444007fe = () => interopDefault(import('..\\pages\\pengumuman\\index.vue' /* webpackChunkName: "pages/pengumuman/index" */))
const _5360d183 = () => interopDefault(import('..\\pages\\privacy\\index.vue' /* webpackChunkName: "pages/privacy/index" */))
const _c1622f02 = () => interopDefault(import('..\\pages\\proyekPerubahan\\index.vue' /* webpackChunkName: "pages/proyekPerubahan/index" */))
const _4f866754 = () => interopDefault(import('..\\pages\\susunan\\index.vue' /* webpackChunkName: "pages/susunan/index" */))
const _3619c782 = () => interopDefault(import('..\\pages\\landing-page-1\\about-1.vue' /* webpackChunkName: "pages/landing-page-1/about-1" */))
const _35fd9880 = () => interopDefault(import('..\\pages\\landing-page-1\\about-2.vue' /* webpackChunkName: "pages/landing-page-1/about-2" */))
const _a71c13ae = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'current-menu-item',
  linkExactActiveClass: 'current-menu-item',
  scrollBehavior,

  routes: [{
    path: "/alumniTerbaik",
    component: _8e04bf32,
    name: "alumniTerbaik"
  }, {
    path: "/landing-page-1",
    component: _7c220820,
    name: "landing-page-1"
  }, {
    path: "/pengumuman",
    component: _444007fe,
    name: "pengumuman"
  }, {
    path: "/privacy",
    component: _5360d183,
    name: "privacy"
  }, {
    path: "/proyekPerubahan",
    component: _c1622f02,
    name: "proyekPerubahan"
  }, {
    path: "/susunan",
    component: _4f866754,
    name: "susunan"
  }, {
    path: "/landing-page-1/about-1",
    component: _3619c782,
    name: "landing-page-1-about-1"
  }, {
    path: "/landing-page-1/about-2",
    component: _35fd9880,
    name: "landing-page-1-about-2"
  }, {
    path: "/",
    component: _a71c13ae,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
